module.exports = {
  stories: ["../stories/*.stories.tsx", "../src/**/*.stories.tsx"],
  addons: ["@storybook/addon-essentials"],
};
