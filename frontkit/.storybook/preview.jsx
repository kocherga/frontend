import * as React from "react";

import { GlobalStyle } from "../src";

export const decorators = [
  (Story) => (
    // StrictMode won't be necessary after 6.1 gets released (see https://github.com/storybookjs/storybook/issues/12734)
    <React.StrictMode>
      <GlobalStyle />
      <Story />
    </React.StrictMode>
  ),
];
