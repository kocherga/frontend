import React from 'react';
import styled from 'styled-components';

import { storiesOf } from '@storybook/react';

import { A, colors, Column, HR, RichText, Row } from './';

export default {
  title: "Общее/Цвета",
};

const ColorBox = styled.div`
  height: 50px;
  flex: 1;
  background-color: ${(props) => props.color};
  text-align: center;
  line-height: 50px;
`;

export const AllColors = () => (
  <Column stretch>
    <Row stretch>
      <ColorBox color={colors.primary[100]}>primary/100</ColorBox>
      <ColorBox color={colors.primary[300]}>primary/300</ColorBox>
      <ColorBox color={colors.primary[500]}>primary/500</ColorBox>
      <ColorBox color={colors.primary[700]}>primary/700</ColorBox>
      <ColorBox color={colors.primary[900]}>primary/900</ColorBox>
    </Row>
    <Row stretch>
      <ColorBox color={colors.grey[100]}>grey/100</ColorBox>
      <ColorBox color={colors.grey[300]}>grey/300</ColorBox>
      <ColorBox color={colors.grey[800]}>grey/800</ColorBox>
      <ColorBox color={colors.grey[900]}>grey/900</ColorBox>
    </Row>
    <Row stretch>
      <ColorBox color={colors.accent[100]}>accent/100</ColorBox>
      <ColorBox color={colors.accent[300]}>accent/300</ColorBox>
      <ColorBox color={colors.accent[500]}>accent/500</ColorBox>
      <ColorBox color={colors.accent[700]}>accent/700</ColorBox>
      <ColorBox color={colors.accent[900]}>accent/900</ColorBox>
    </Row>
  </Column>
);
AllColors.storyName = "Все цвета";
