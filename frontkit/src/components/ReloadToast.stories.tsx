import React from 'react';

import { ReloadToast } from './ReloadToast';

export default {
  title: "Components/ReloadToast",
  component: ReloadToast,
};

export const Basic = () => <ReloadToast />;
