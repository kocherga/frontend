module.exports = {
  transform: {
    "\\.[jt]sx?$": ["babel-jest", { configFile: "./babel.jest.config.json" }],
  },
};
