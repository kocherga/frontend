export const highlight = '#ffffb3';

export const hues = {
  grey: 208,
  blue: 210,
  red: 355,
};

export const primary = {
  100: `hsl(${hues.blue}, 80%, 95%)`,
  300: `hsl(${hues.blue}, 55%, 80%)`,
  500: `hsl(${hues.blue}, 55%, 55%)`,
  700: `hsl(${hues.blue}, 55%, 35%)`,
  900: `hsl(${hues.blue}, 55%, 20%)`,
};

export const link = `hsl(${hues.blue}, 80%, 50%)`;

export const grey = {
  100: `hsl(208, 20%, 97%)`,
  200: `hsl(208, 12%, 92%)`,
  300: `hsl(208, 12%, 80%)`,
  400: `hsl(208, 12%, 70%)`,
  500: `hsl(208, 12%, 58%)`,
  600: `hsl(208, 12%, 43%)`,
  700: `hsl(208, 12%, 38%)`,
  800: `hsl(208, 12%, 33%)`,
  900: `hsl(208, 12%, 28%)`,
};

export const accent = {
  100: `hsl(0, 100%, 90%)`,
  300: `hsl(0, 100%, 80%)`,
  500: `hsl(0, 100%, 74%)`,
  700: `hsl(0, 100%, 70%)`,
  900: `hsl(0, 60%, 60%)`,
};
