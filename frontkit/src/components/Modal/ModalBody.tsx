import styled from 'styled-components';

const ModalBody = styled.div`
  padding: 6px 10px;
  overflow: auto;
`;

export default ModalBody;
