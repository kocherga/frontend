import styled from 'styled-components';

import * as colors from '../../colors';

const ModalFooter = styled.footer`
  padding: 6px 10px;
  border-top: 1px solid ${colors.grey[200]};
`;

export default ModalFooter;
